FROM openjdk:17-jdk-alpine
MAINTAINER OlegKolesnikov
COPY target/demo-0.0.1-SNAPSHOT.jar  SNAPSHOT2.jar
ENTRYPOINT ["java","-jar","/SNAPSHOT2.jar"]